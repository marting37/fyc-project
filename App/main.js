const { app, BrowserWindow } = require('electron');
const path = require('path');
const os = require("os");
const {Notification,nativeTheme } = require('electron');

function showNotification (){
    const notification = {
        title:'Titre',
        body:'Notification test'
    }
    new Notification(notification).show()
}

const { ipcMain } = require('electron')









console.log(os.networkInterfaces());
function createWindow () {
    const win = new BrowserWindow({

        width: 1000,
        height: 700,
        frame: process.platform === 'darwin',
        titleBarStyle: "hidden",
        webPreferences: {
            nodeIntegration: true,      
            enableRemoteModule: true,
            preload: path.join(__dirname, 'preload.js'),
        }
    });

    win.loadFile('index.html');


    win.setProgressBar(0.5)

// et charger le fichier index.html de l'application.
    win.loadFile('index.html');

   ipcMain.handle('dark-mode:toggle', ()=>{
       if(nativeTheme.shouldUseDarkColors){
            nativeTheme.themeSource = "light"
       }else{
            nativeTheme.themeSource = "dark"
       }
       return nativeTheme.shouldUseDarkColors
   })
    

    // Ouvre les DevTools.

    win.webContents.openDevTools();

   
}

app.whenReady().then(createWindow);



// Cette méthode sera appelée quant Electron aura fini
// de s'initialiser et prêt à créer des fenêtres de navigation.
// Certaines APIs peuvent être utilisées uniquement quant cet événement est émit.
app.whenReady().then(createWindow).then(showNotification);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {

    let win;
    if (win === null) {
        createWindow()
    }

});









