var the = {
    use_codemirror: (!window.location.href.match(/without-codemirror/)),
    beautify_in_progress: false,
    editor: null // codemirror editor
};


function unpacker_filter(source) {
    var trailing_comments = '',
        comment = '',
        unpacked = '',
        found = false;

    // couper les commentaires
    do {
        found = false;
        if (/^\s*\/\*/.test(source)) {
            found = true;
            comment = source.substr(0, source.indexOf('*/') + 2);
            source = source.substr(comment.length).replace(/^\s+/, '');
            trailing_comments += comment + "\n";
        } else if (/^\s*\/\//.test(source)) {
            found = true;
            comment = source.match(/^\s*\/\/.*/)[0];
            source = source.substr(comment.length).replace(/^\s+/, '');
            trailing_comments += comment + "\n";
        }
    } while (found);

    var unpackers = [P_A_C_K_E_R, Urlencoded, JavascriptObfuscator /*, MyObfuscate*/ ];
    for (var i = 0; i < unpackers.length; i++) {
        if (unpackers[i].detect(source)) {
            unpacked = unpackers[i].unpack(source);
            if (unpacked != source) {
                source = unpacker_filter(unpacked);
            }
        }
    }

    return trailing_comments + source;
}


function beautify() {
    if (the.beautify_in_progress) return;

    the.beautify_in_progress = true;

    var source = the.editor ? the.editor.getValue() : $('#source').val(),
        output,
        opts = {};

    opts.indent_size = $('#tabsize').val();
    opts.indent_char = opts.indent_size == 1 ? '\t' : ' ';
    opts.max_preserve_newlines = $('#max-preserve-newlines').val();
    opts.preserve_newlines = opts.max_preserve_newlines !== "-1";
    opts.keep_array_indentation = $('#keep-array-indentation').prop('checked');
    opts.break_chained_methods = $('#break-chained-methods').prop('checked');
    opts.indent_scripts = $('#indent-scripts').val();
    //opts.brace_style = $('#brace-style').val() + ($('#brace-preserve-inline').prop('checked') ? ",preserve-inline" : "");
    opts.space_before_conditional = $('#space-before-conditional').prop('checked');
    opts.unescape_strings = $('#unescape-strings').prop('checked');
    opts.jslint_happy = $('#jslint-happy').prop('checked');
    opts.end_with_newline = $('#end-with-newline').prop('checked');
    opts.wrap_line_length = $('#wrap-line-length').val();
    opts.indent_inner_html = $('#indent-inner-html').prop('checked');
    opts.comma_first = $('#comma-first').prop('checked');
    opts.e4x = $('#e4x').prop('checked');

    if (looks_like_html(source)) {
        //Si le code ressemble à de l'html on execute html_beautify()
        output = html_beautify(source, opts);
    } else {
        //Si le code ressemble à du js on execute js_beautify()
        output = js_beautify(source, opts);
    }
    if (the.editor) {
        the.editor.setValue(output);
    } else {
        $('#editorcode').val(output);
    }

    the.beautify_in_progress = false;
}

function looks_like_html(source) {
    var html = source.replace(/^[ \t\n\r]+/, '');
    return html && (html.substring(0, 1) === '<');
}


try {
    $(function () {

        var default_text =
            "/* Ceci est juste un exemple de démonstration. Collez votre vrai code ici. */\n\n";
        var textArea = $('#editorcode')[0];
        default_text += $('#hidden_default_text').text();

        if (the.use_codemirror && typeof CodeMirror !== 'undefined') {
            the.editor = CodeMirror.fromTextArea(textArea, {
                theme: 'monokai', //Choisir le theme par default
                lineNumbers: true, // Numero de ligne
            });
            the.editor.focus();

            the.editor.setValue(default_text);
            $('.CodeMirror').click(function () {
                if (the.editor.getValue() == default_text) {
                    the.editor.setValue('');
                }
            });
        }

        $('.submit').click(beautify);

        //Gestion du theme
        var editor = the.editor;
        var input = document.getElementById("select_theme"); // Bouton select pour changer de theme

        input.onchange = () => {
            var theme = input.options[input.selectedIndex].textContent;
            editor.setOption("theme", theme);
            location.hash = "#" + theme;
        };

        var choice = (location.hash && location.hash.slice(1)) ||
            (document.location.search &&
                decodeURIComponent(document.location.search.slice(1)));
        if (choice) {
            input.value = choice;
            editor.setOption("theme", choice);
        }
        CodeMirror.on(window, "hashchange", function () {
            var theme = location.hash.slice(1);
            if (theme) {
                input.value = theme;
                // selectTheme();
            }
        });

        // Code compressé
        var btn_compress = document.getElementById("btn_compress");
        if (btn_compress) {
            btn_compress.onclick = () => {
                var code_content = editor.getValue();
                if (code_content == '') {
                    code_content = editor.toTextArea();
                }
                try {
                    var compressResult = "";
                    if ($('#btn_compress').data('json')) {
                        code_content = code_content.replace('/* Ceci est juste un exemple de démonstration. Collez votre vrai code ici. */', '');
                        compressResult = compressJSON(code_content);
                    } else {
                        compressResult = code_content.split("\n").join(" ").replace(/\s+/g, " ");
                    }
                    editor.setValue(compressResult);
                } catch (e) {
                    // Si il y a une erreur
                    console.log(e);
                }

            }
        }
    });
} catch (e) {
    // Si il y a une erreur
    console.log(e);
}