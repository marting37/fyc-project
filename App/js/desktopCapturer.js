const { desktopCapturer, remote } = require('electron');
const { writeFile } = require('fs');
const { dialog, Menu } = remote;

let mediaRecorder;
const recordedChunks = [];

const videoElement = document.querySelector('video');

const startBtn = document.getElementById('startBtn');
startBtn.onclick = e => {
    mediaRecorder.start();
    startBtn.innerText = 'Enregistrement en cours';
};
const stopBtn = document.getElementById('stopBtn');
stopBtn.onclick = e => {
    mediaRecorder.stop();
    startBtn.innerText = 'Démarrer';
};

const selectSourceBtn = document.getElementById('selectSourceBtn');
selectSourceBtn.onclick = getSources;

async function getSources(){
    //Recupération des sources avec desktopCapturer
    const inputSources = await desktopCapturer.getSources({
        types:['windows', 'screen']
    });
    //Création du Popup des sources
    const optionsMenu = Menu.buildFromTemplate(
        inputSources.map(source => {
            return {
                label: source.name,
                click: () => selectSource(source)
            }
        })
    );

    optionsMenu.popup()
}

async function selectSource(source) {

    selectSourceBtn.innerText = source.name;
  
    const constraints = {
      audio: false,
      video: {
        mandatory: {
          chromeMediaSource: 'desktop',
          chromeMediaSourceId: source.id
        }
      }
    };
  
    // Create a Stream
    const stream = await navigator.mediaDevices
      .getUserMedia(constraints);
  
    // Preview the source in a video element
    videoElement.srcObject = stream;
    videoElement.play();
  
    // Create the Media Recorder
    mediaRecorder = new MediaRecorder(stream, { mimeType: 'video/webm; codecs=vp9' });
  
    // Register Event Handlers
    mediaRecorder.ondataavailable = handleDataAvailable;
    mediaRecorder.onstop = handleStop;
  
    // Updates the UI
  }
  
  // Captures all recorded chunks
  function handleDataAvailable(e) {
    recordedChunks.push(e.data);
  }
  
  // Saves the video file on stop
  async function handleStop(e) {
    const blob = new Blob(recordedChunks, {
      type: 'video/webm; codecs=vp9'
    });
  
    const buffer = Buffer.from(await blob.arrayBuffer());
  
    const { filePath } = await dialog.showSaveDialog({
      buttonLabel: 'Save video',
      defaultPath: `desktopCapturer-${Date.now()}.webm`
    });
  
    if (filePath) {
      writeFile(filePath, buffer, () => console.log('video saved successfully!'));
    }
  
  }