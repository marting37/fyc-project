
 
window.addEventListener('DOMContentLoaded', () => {
    if (process.platform !== 'darwin') {
        const customTitlebar = require('custom-electron-titlebar');
        const { Menu, MenuItem } = require('electron').remote;
        let titleBar = new customTitlebar.Titlebar({
            backgroundColor: customTitlebar.Color.fromHex('#FF5733'),
            icon: './image/icon.png'
        });
        //titleBar.updateTitle('Toolbox');
        const menu = new Menu();
        menu.append(new MenuItem({
            label: 'Item 1',
            submenu: [
                {
                    label: 'Subitem 1',
                    click: () => console.log('Click on subitem 1')
                },
                {
                    type: 'separator'
                }
            ]
        }));
        
        menu.append(new MenuItem({
            label: 'Item 2',
            submenu: [
                {
                    label: 'Subitem checkbox',
                    type: 'checkbox',
                    checked: true
                },
                {
                    type: 'separator'
                },
                {
                    label: 'Subitem with submenu',
                    submenu: [
                        {
                            label: 'Submenu &item 1',
                            accelerator: 'Ctrl+T'
                        }
                    ]
                }
            ]
        }));
        
        titleBar.updateMenu(menu);
    }
})