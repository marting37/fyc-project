const { app, BrowserWindow } = require('electron');

const os = require("os");

console.log(os.networkInterfaces());
console.log(os.platform());
function createWindow () {
    // Cree la fenetre du navigateur.
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });

// et charger le fichier index.html de l'application.
    win.loadFile('index.html');

    // Ouvre les DevTools.
    win.webContents.openDevTools();
}

// Cette méthode sera appelée quant Electron aura fini
// de s'initialiser et prêt à créer des fenêtres de navigation.
// Certaines APIs peuvent être utilisées uniquement quant cet événement est émit.
app.whenReady().then(createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    let win;
    if (win === null) {
        createWindow()
    }
});
